Puppet::Type.newtype(:duo_config) do
  @doc = "Get duo configuration from wallet and apply options"

  ensurable do
    desc "Get a file from wallet or remove it"
    defaultvalues
    defaultto(:present)
  end

  newparam(:verify, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Keep configuration file synced with wallet."
    defaultto(:false)
  end

  newparam(:host_name) do
    desc "Server name"   
    defaultto(Facter.value(:networking)['fqdn'])
  end

  newparam(:name) do
    desc "The local file to save configuration to"
    defaultto("/etc/security/pam_duo_" + Facter.value(:networking)['fqdn'] + ".conf")

    validate do |path|
      raise ArgumentError, 'path must be a fully qualified path' unless absolute_path? path
    end
  end

  newparam(:source) do
    desc "The local file containing downloaded wallet object"

    validate do |path|
      raise ArgumentError, 'path must be a fully qualified path' unless absolute_path? path
    end
  end

  newparam(:ikey) do
    desc "Integration key"

    validate do |value|
      unless /^[A-Z0-9]{20}$/.match(value)
        raise ArgumentError, "#{value} does not look like a valid key."
      end
    end
  end 

  newparam(:skey) do
    desc "Secret key"

    validate do |value|
      unless /^[a-zA-Z0-9]{40}$/.match(value)
        raise ArgumentError, "#{value} does not look like a secret key."
      end
    end
  end 

  newparam(:host) do
    desc "API host"

    validate do |value|
      unless /^[a-zA-Z0-9\-\.]+\.duosecurity\.com$/.match(value)
        raise ArgumentError, "#{value} does not look like a valid API server name."
      end
    end
  end 

  newparam(:auth_keytab) do
    desc "Keytab used to authenticate to wallet"
    defaultto('/etc/krb5.keytab')

    validate do |path|
      raise ArgumentError, 'Path must be a fully qualified path' unless absolute_path? path
    end
  end 

  newparam(:auth_principal) do
    desc "Principal in auth_keytab used to authenticate to wallet"

    validate do |value|
      unless /^host\/[0-9a-zA-Z\.\-]+$/.match(value)
        raise ArgumentError, "Name #{value} does not look like a host principal."
      end
    end    
  end 

  newparam(:failmode) do
    desc "Deny or allow access when Duo PAM module fails."
    newvalues(:safe, :secure)
  end 

  newparam(:http_proxy) do
    desc "Proxy server"
  end 

  newparam(:groups, :array_matching => :all) do
    desc "Duo 2FA is required only for these groups."

    validate do |value|
      raise ArgumentError, "Number of prompts must be between 1 and 3." unless value.is_a?(Array)
    end
  end 

  newparam(:autopush, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Automatically send a push to the mobile device."
    newvalues(:true, :false)  
  end

  newparam(:prompts) do
    desc "How many times to repeat a prompt."

    # make sure we deal with string representations rather then integers
    munge do |value|
      value = value.to_s
    end

    validate do |value|
      unless /^(1|2|3)$/.match(value.to_s)
        raise ArgumentError, "Number of prompts must be between 1 and 3."
      end
    end
  end 

  newparam(:fallback_local_ip, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Fall back to using local IP if it cannot be resolved."
    newvalues(:true, :false)  
  end

  newparam(:https_timeout) do
    desc "Time to wait for https responses from Duo service."

    # make sure we deal with string representations rather then integers
    munge do |value|
      value = value.to_s
    end

    validate do |value|
      unless /^[0-9]+$/.match(value.to_s)
        raise ArgumentError, "Timeout must be an integer"
      end
    end
  end 

  newparam(:send_gecos, :boolean => true, :parent => Puppet::Parameter::Boolean) do
    desc "Use gecos field as username"
    newvalues(:true, :false)  
  end

  newparam(:gecos_username_pos) do
    desc "Specify a position of a username in the gecos field comprised of a character separated values."

    # make sure we deal with string representations rather then integers
    munge do |value|
      value = value.to_s
    end

    validate do |value|
      unless /^[0-9]+$/.match(value.to_s)
        raise ArgumentError, "Position must be an integer"
      end
    end
  end 

  newparam(:gecos_delim) do
    desc "Specify a custom separator for the values in gecos field"

    validate do |value|
      unless /^[^\s]$/.match(value)
        raise ArgumentError, "Delimiter must be a single character."
      end
    end
  end 

  # require any parent directory be created first
  autorequire :file do
    [ File.dirname(self[:name]) ]
  end

end
