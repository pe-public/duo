Puppet::Type.type(:duo_config).provide(:duo_config) do
  desc "Manage Duo configuration files"
  confine :osfamily => [:redhat, :debian]
  optional_commands  :kstart => "/usr/bin/k5start"


#### Initialize a property hash
##############################

  def initialize(*args)
    super(*args)
    @property_flush = {}

    # list of all parameters
    @vp = [
      :host,                # API host name, string
      :failmode,            # Fail opened or closed, boolean
      :http_proxy,          # Proxy host name and port, string
      :ikey,                # Integration key, string
      :skey,                # Secret key, string
      :groups,              # List of groups to authenticate, array
      :autopush,            # Send push without asking, boolean
      :prompts,             # How many times to retry authentication, integer
      :https_timeout,       # How long until connection to API host times out, integer
      :send_gecos,          # Send gecos field as user name, boolean
      :gecos_username_pos,  # Position of a user name in gecos field, integer
      :gecos_delim,         # Delimiter of the fields inside gecos, string (single character)
      :fallback_local_ip    # Use local IP if detecion of a public IP fails, boolean
    ]
  end



#### does resource exist?
##############################

  def exists?
    file_exists = File.file?(@resource[:name])

    # That's all we need to do if a configuration is set to absent
    return file_exists if @resource[:ensure] == :absent

    # Ignore verification option if keys and host are given in the resource
    verify = (@resource[:verify] and
       (@resource[:ikey].nil? or @resource[:skey].nil? or @resource[:host].nil?))

    # Get the data about main duo parameters, ikey, skey and host
    # from a source of truth. If "source" is defined, then it is a
    # file, otherwise it is a wallet object.
    if !file_exists or verify
      if @resource[:source]
        wallet_data = parse_config(File.read(@resource[:source]))
      else
        if !File.file?('/usr/bin/k5start')
          raise Puppet::Error, "duo_config resource needs k5start program to read wallet objects. Please install 'kstart' package."
        end
        wallet_data = parse_config(read_wallet)
      end

      # Here consider only keys and a host parameters
      wallet_data.keep_if {|k,v| [:ikey,:skey,:host].include?(k)}
    end

    if file_exists
      exists = true

      # read existing config file into a hash
      file_data = parse_config(File.read(@resource[:name]))

      # File exists. Take the properties in it as a starting point.
      @property_flush = file_data

      # Compare data found in wallet object to the data read from file.
      # If there is a mismatch update proprty hash with the data from wallet.
      if verify
        wallet_data.each do |k,v|
          if file_data[k] != v
            Puppet.notice("#{@resource.instance_variable_get(:@path)}/#{k}: changed from '#{file_data[k]}' to '#{wallet_data[k]}'")
            exists = false
          end
        end
        @property_flush = file_data.merge(wallet_data) unless exists
      end

      # Compare data found in a file to the data in the resource.
      # If a file contains more data, remove it from a property hash.
      # Wallet data is processed above separately since it comes from
      # a different source.
      file_data.each do |k,v|
        unless [:ikey,:skey,:host].include?(k)
          if @resource[k].nil?
            @property_flush.delete(k)
            Puppet.notice("#{@resource.instance_variable_get(:@path)}/#{k}: changed from '#{v}' to 'default'")
            exists = false
          end
        end
      end
    else
      # file does not exist, so we just fill the property hash
      # with the data from wallet
      @property_flush = wallet_data
      exists = false
    end

    # rewrite the file if it is not there or anything changed
    unchanged = !update_properties()
    exists = (exists and unchanged)

    return exists
  end



#### create resource
##############################

  def create
    # flush does it
  end



#### destroy resource
##############################

  def destroy
    File.unlink(@resource[:name])
  end



#### flush resource
##############################

  def flush
    write_config unless @property_flush.empty?
  end



#### helper functions
##############################

  # Convert boolean to yes/no and back
  def yesno(value)
    if [ true, false ].include? value
      value == true ? 'yes': 'no'
    elsif value.is_a? String
      (value.downcase =~ /yes/) ? true : false
    else
      value
    end
  end


  def read_wallet
    if @resource[:auth_principal].nil?
      kstart("-Uqf", @resource[:auth_keytab], 
          "--", "/usr/bin/wallet",
          "get", "duo-pam", @resource[:host_name])
    else
      kstart("-qf", @resource[:auth_keytab], @resource[:auth_principal], 
          "--", "/usr/bin/wallet", 
          "get", "duo-pam", @resource[:host_name])
    end
  end


  # read the contents of a file into a hash making sure only valid properties are read.
  def parse_config(config)
    needs_update = false

    # split the contents of a configuration into lines and delete all spaces and empty lines
    lines = config.split("\n").map(&:chomp).map {|line| line.delete(' ')}.reject{ |line| line.empty? }

    # convert lines array into a hash with keys as symbols
    options = Hash[lines.map {|line| line.split('=')}].inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}

    # strip comments and section headers
    no_comment = options.reject {|k,v| k =~ /^(#|\[)/}

    # consider only known parameters
    options.keep_if {|k,v| @vp.include?(k)}

    # see if there are any illegal keys given
    if no_comment != options
      raise Puppet::Error, "Unknown parameters found in a configuraion file."
    end

    # convert comma separated to array
    options[:groups] = options[:groups].split(',') if options[:groups]

    # some values have to be symbols as well
    # yes/no convert to :true and :false
    options.each do |k,v|
      options[k] = v.to_sym if ['secure','safe'].include?(v)
      options[k] = yesno(v) if ['yes', 'no'].include?(v)
    end

    return options
  end


  # write a contents of a property hash to a config file
  def write_config
    open(@resource[:name], 'w') do |f|

      # put a comment at the top of the file with the host name.
      # we'll use it to track host name changes and force an update
      # if it does change.
      f.puts "[duo]"
      @property_flush.each do |key, value|
        case value
        when Array
          str_value = value.join(',')
        when (true or false)
          str_value = yesno(value)
        else
          str_value = value.to_s
        end
        f.puts "#{key} = #{str_value}"
      end
    end

    # Maintain file mode
    if File.stat(@resource[:name]).mode != 0o600
      File.chmod(0o600, @resource[:name])
    end
  end

  def update_properties
    changed = false
    @vp.each do |k|
      if @resource[k] and (@resource[k] != @property_flush[k])
        from_value = @property_flush[k] ? @property_flush[k] : 'default'
        Puppet.notice("#{@resource.instance_variable_get(:@path)}/#{k}: changed from '#{from_value}' to '#{@resource[k]}'")
        @property_flush[k] = @resource[k]
        changed = (changed or true)
      end
    end
    
    # return true if any of the properties has changed.
    # if they did, the file should be rewritten.
    return changed
  end

end
