Duo module
==========

This module installs Duo PAM module and provides a resource to create Duo
configuration files.

## Installation

To install Duo PAM module just include the module

```puppet
include duo
```

The duo class is rudimentary and assumes that repositories where Duo can be
found are managed elsewhere.

## Configuration

### Duo packages

By default the `duo` Pupet module will install the package with the name
`duo_unix`. The `duo` Puppet module does _not_ do any repository
configuration, so it is up to another part of your Puppet code to ensure
that the `duo_unix` package is available.

If you would rather install the official Debian project distributed
versions of the Duo software (which do _not_ use the package name `duo_unix`), 
call the `duo` class with the parameter `native_package` set to `true`.

### The `duo_config` resource

`duo_config` resource allows to create cusom Duo configurations based on the
information pulled from `wallet` application. Wallet provides file objects with
the basic configuration for each server giving individual integration and
secret keys as well as the address of the API host to send commands to:

```
[duo]
ikey = XXXXXXXXXXXXXXXXXXXX
skey = YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
host = api-531b0865.duosecurity.com
```

The module downloads the wallet object and parses the file. If there is no
local copy of the configuration file, it creates it based on the parsed values
from wallet as well as parameters provided by resource instance in puppet. If
desired the module can verify that the values in existing configuration files
match to what the wallet has for the host. Naturally changing the rest of the
parameters update the local configuration file as well.

Besides `duo_config` resource dependency on `wallet` application, it also needs 
`k5start` command to read and verify wallet objects. If the resource is called
with a source file name, it does not need either of these applications.

### Wallet-related parameters

`duo_config` resource has the following parameters, controlling the acquision
of the secrets from `wallet` and others not affecting the behavior of the PAM
module itself.

#### name

Name of the configuration file to generate. Mandatory, should be an absolute
path.

#### host_name

The name of the server, which the code is running on being also a name of the
duo-pam wallet object containing three mandatory parameters of Duo integration,
_key_, _skey_ and _host_. Optional, defaults to the fqdn of the host.

#### ensure

True or false for the configuration file. Optional, defaults to _present_.

#### verify

If true, ensures that three parameters in the configuration, _skey_, _ikey_ and
_host_, are matching a given source, a wallet object or a file specified as
`source`. Option is ingored if all three are given as a resource parameters.
Optional, defaults to _false_.

#### auth_principal

A principal used to authenticate to wallet. Useful parameter to specify on the
hosts with multiple names having keys for each of them in the host keytab.
Optional, defaults to the first key in the keytab.

#### auth_keytab

Keytab containing `auth_principal`. Optional, defaults to _/etc/krb5.keytab_.

#### source

A path to a downloaded wallet object with duo configuration data. Can be used
to create configurations with different options out of a common minimal data.
If `verify` option is set to _true_, the particular configurations are verified
against the common data in a file at every puppet run. Optional, defaults to
_undef_.

### PAM module parameters

These are parameters of the resource available to tune authentication settings
of the Duo PAM module.

#### ikey

Integration key for Duo API. Provided by Wallet integration, `source` text file
or given as a resource parameter.

#### skey

Secret key for Duo API. Provided by Wallet integration, `source` text file or
given as a resource parameter.

#### host

A host running Duo API. Provided by Wallet integration, `source` text file or
given as a resource parameter.

#### groups

If specified, Duo authentication is required only for users whose primary group
or supplementary group list matches one of the space-separated pattern lists.

A pattern consists of zero or more non-whitespace characters, "\*" (a wild card
that matches zero or more characters), or "?" (a wildcard that matches exactly
one character).

A pattern-list is a comma-separated list of patterns. Patterns within
pattern-lists may be negated by preceding them with an exclamation mark ("!").
For example, to specify Duo authentication for all users (except those that are
also admins), and for guests:

```puppet
groups => [ 'users', '!wheel', '!*admin', 'guests' ]
```

#### failmode

On service or configuration errors that prevent Duo authentication, fail "safe"
(allow access) or "secure" (deny access). The default is "safe".

#### http_proxy

Use the specified HTTP proxy. If the HTTP proxy requires authentication,
include the credentials in the proxy URL. Example format:

```
http_proxy => 'http://username:password@proxy.example.org:8080'
```

#### autopush

Either `true` or `false`. Default is `false`. If `true`, Duo Unix will
automatically send a push login request to the user's phone, falling back on a
phone call if push is unavailable. Note that this effectively disables passcode
authentication. If `false`, the user will be prompted to choose an
authentication method.

When configured with `autopush => true`, we recommend setting `prompts = '1'`.

#### prompts

If a user fails to authenticate with a second factor, Duo Unix will prompt the
user to authenticate again. This option sets the maximum number of prompts that
Duo Unix will display before denying access. Must be '1', '2', or '3'. Default
is '3'.

For example, when `prompts => '1'`, the user will have to successfully
authenticate on the first prompt, whereas if `prompts => '2'`, if the user
enters incorrect information at the initial prompt, he/she will be prompted to
authenticate again.

When configured with `autopush => true`, we recommend setting `prompts => '1'`.

#### fallback_local_ip

Duo Unix reports the IP address of the authorizing user, for the purposes of
authorization and whitelisting. If Duo Unix cannot detect the IP address of the
client, setting fallback_local_ip = yes will cause Duo Unix to send the IP
address of the server it is running on.

#### https_timeout

Set to the number of seconds to wait for HTTPS responses from Duo Security. If
Duo Security takes longer than the configured number of seconds to respond to
the preauth API call, the configured failmode is triggered. Other network
operations such as DNS resolution, TCP connection establishment, and the SSL
handshake have their own independent timeout and retry logic.

Default is 0, which disables the HTTPS timeout.

If you specify an https_timeout value for Duo Unix, be sure that you do not set
a conflicting socket timeout.

#### send_gecos

Sends the entire GECOS field as the Duo username.

Default is `false`; the GECOS field is not used or parsed.

If you specify `gecos_username_pos` and gecos_delim, this setting is ignored.

#### gecos_username_pos

Specify this option to select what position from the GECOS field will be used
as the username. Positions are separated by whatever you specify in gecos_delim
or the default delimeter, a comma (,).

For example, if the `/etc/passwd` entry for a user is:

```
test_user:x:UID:GID:gecos1,gecos2,gecos3:/home/test_user:/bin/bash
```

Then setting `gecos_username_pos => '2'` sends gecos2 as the Duo username.

If not configured, the GECOS field is not parsed for the username.

Overrides `send_gecos`, if set.

#### gecos_delim

Specify this option to change the default value of the GECOS delimiter from a
comma to another character. The new delimeter specified must be exactly one
character, and must a valid punctuation character other than a colon (:).

For example, if the `/etc/passwd` entry for a user is:

```
test_user:x:UID:GID:gecos1/gecos2/gecos3:/home/test_user:/bin/bash
```

Then setting `gecos_username_pos => '3'` and `gecos_delim => '/'` sends gecos3
as the Duo username.

If not configured, the default comma (,) GECOS field delimiter is used.

## Examples

Creates a configuration file `/etc/security/pam_duo_crc-web2.stanford.edu.conf`
with duo keys and API host taken from wallet. Authenticates to wallet with a
principal `host/server.stanford.edu` located in `/etc/krb5.keytab` keytab file.
Sets proxy server to `tcg-proxy.sanford.edu` on port `17123`. Enforces Duo
authentication only to the groups `admin`, `dim` and `sum`. The user name is
taken from gecos field rather than the actual user name. If authentication
fails, login is denied. Connection to the API server times out in 12 seconds.

```puppet
duo_config { '/etc/security/pam_duo.conf':
  ensure         => 'present',
  verify         => true,
  auth_principal => 'host/server.stanford.edu'
  http_proxy     => 'https://tcg-proxy.stanford.edu:17123',
  groups         => [ 'admin', 'dim', 'sum' ],
  send_gecos     => true,
  failmode       => 'secure',
  https_timeout  => 12,
}
```

## Notes

* The owner and group of the configuration file are the same as the process
  running the module, i.e. puppet, thus making it root:root. Ownership is not
  enforced by module.
* Mode of the configuraion file is set to be 600 and is maintened by the
  module.
* To facilitate automatic update of the keys on host rename, the hostname can
  be made a part of the configuration file
  file name and referred as such in a PAM configuration file:

  ```
  duo_config { "/etc/security/pam_duo_${::hostname}_sunet.conf": }
  ```

  and in `/etc/pam.d/sshd` template file something like:

  ```
  auth    sufficient     pam_duo.so conf=/etc/security/pam_duo_<$= @hostname =>_sunet.conf
  ```

  This way when the server name changes, new configuration file with different
keys would be generated and inserted into PAM sshd configuration. Old
configuration files would be left behind unreferenced from anywhere and would
have to be removed manually.

## References

[Duo UNIX
documentation](https://duo.com/docs/duounix#duo-configuration-options)
