# install duo pam module
# assumes that repositories are managed elsewhere

# $native_duo_package: If true install the package "duo_unix" package. If false,
#                      and the OS is Debian, then install the Debian-distributed
#                      Duo packages. Must be in sync with the identically named
#                      parameter in the pam module.
#                      Default: true
#

class duo (
  Enum['present', 'absent'] $ensure = 'present',
  Boolean $native_duo_package = false,
) {
  # os-dependent defaults
  include duo::params

  $duo_pkgs = $duo::params::duo_pkgs
  package { $duo_pkgs: ensure => $ensure }
}
