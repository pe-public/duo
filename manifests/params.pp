# defaults for duo packages
#
class duo::params {

  case $facts['os']['family'] {
    'Debian': {
      $duo_pkgs = $duo::native_duo_package ? {
        true  => ['libduo3', 'libpam-duo'],
        false => ['duo-unix']
      }
    }
    'RedHat': {
      $duo_pkgs = ['duo_unix']
    }
  }

}
